package com.rawsanj.adminlte.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pages")
public class ExamplesController {
    @RequestMapping("/examples/blank")
    public String examplesBlank() {
        return "examples/blank";
    }

    @RequestMapping("/forms/advanced")
    public String formsAdv() {
        return "forms/advanced";
    }
    @RequestMapping("/forms/general")
    public String formsGeneral() {
        return "forms/general";
    }
}
